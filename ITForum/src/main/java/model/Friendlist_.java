package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-04-20T02:48:49.540+0300")
@StaticMetamodel(Friendlist.class)
public class Friendlist_ {
	public static volatile SingularAttribute<Friendlist, Integer> id;
	public static volatile SingularAttribute<Friendlist, UserAccount> userAccount1;
	public static volatile SingularAttribute<Friendlist, UserAccount> userAccount2;
}
