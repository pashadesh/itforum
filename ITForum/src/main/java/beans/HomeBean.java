package beans;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import model.Section;
import services.SectionService;
import services.StatusService;

@Named
@ViewScoped
@ResourceDependencies({
    @ResourceDependency(name = "fa/font-awesome.css", library ="primefaces" ),
    @ResourceDependency(name = "components.css", library ="primefaces" ),
    @ResourceDependency(name = "theme.css", library ="primefaces-bootstrap" )
})
public class HomeBean implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = -3090403837405679066L;
    private String hello = "Hello World!";
    private List<Section> sections;
    @Inject
    private SectionService section_service;
    @PostConstruct
    public void init() {
        sections = section_service.getAll();
    }
    
    public String getURLWithContextPath() {
	HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
	return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }
    
    public List<Section> getActiveSections() {
	return sections.stream().filter(x->StatusService.Statuses.APPROVED.getId() == x.getStatusBean().getId().intValue()).collect(Collectors.toList());
    }
    
    public void refresh() {
	sections = section_service.getAll();
    }
    
    /**
     * @return the hello
     */
    public String getHello() {
	return hello;
    }

    /**
     * @param hello the hello to set
     */
    public void setHello(String hello) {
	this.hello = hello;
    }

    /**
     * @return the sections
     */
    public List<Section> getSections() {
        return sections;
    }

    /**
     * @param sections the sections to set
     */
    public void setSections(List<Section> sections) {
        this.sections = sections;
    }
    
}
