package helpers;

import org.omnifaces.util.Faces;

public final class AppSettings {
    public final static int HASH_ROUNDS = 13;
    public final static int HASH_MEMORY = 65536;
    public final static int HASH_THREADS = 2;
    public final static int EMAIL_PORT = 465;
    public final static String EMAIL_LOGIN = "bpv-forum.tech";
    public final static String EMAIL_PASSWORD = "-Gv4Q=Zat94FneB?*CSM%tSj^^!2$BF%MRGu_sj2Vc-#2k8C69%uq#L7m#=#KyDDj%@jJuUyHBbRV6dF7g=kcr*8A%T=hEz#n_Uz?^eJKkrgEmf*2YF!C$B2%dYGr@a-";
    public final static String EMAIL_SERVER = "smtp.yandex.com";
    public final static String EMAIL_FROM = "bpv-forum.tech@yandex.com";
    public final static String EMAIL_THEME = "Подтверждение регистрации на форуме!";

    public static final String createEmailHTML(String userId,
	    String verifyCode) {
	return "<!DOCTYPE html>\r\n" + "<html>\r\n" + "<head>\r\n"
		+ "<title></title>\r\n"
		+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n"
		+ "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n"
		+ "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n"
		+ "<style type=\"text/css\">\r\n"
		+ "    /* FONTS */\r\n" + "    @media screen {\r\n"
		+ "        @font-face {\r\n"
		+ "          font-family: 'Lato';\r\n"
		+ "          font-style: normal;\r\n"
		+ "          font-weight: 400;\r\n"
		+ "          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');\r\n"
		+ "        }\r\n" + "        \r\n"
		+ "        @font-face {\r\n"
		+ "          font-family: 'Lato';\r\n"
		+ "          font-style: normal;\r\n"
		+ "          font-weight: 700;\r\n"
		+ "          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');\r\n"
		+ "        }\r\n" + "        \r\n"
		+ "        @font-face {\r\n"
		+ "          font-family: 'Lato';\r\n"
		+ "          font-style: italic;\r\n"
		+ "          font-weight: 400;\r\n"
		+ "          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');\r\n"
		+ "        }\r\n" + "        \r\n"
		+ "        @font-face {\r\n"
		+ "          font-family: 'Lato';\r\n"
		+ "          font-style: italic;\r\n"
		+ "          font-weight: 700;\r\n"
		+ "          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');\r\n"
		+ "        }\r\n" + "    }\r\n" + "    \r\n"
		+ "    /* CLIENT-SPECIFIC STYLES */\r\n"
		+ "    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }\r\n"
		+ "    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }\r\n"
		+ "    img { -ms-interpolation-mode: bicubic; }\r\n"
		+ "\r\n" + "    /* RESET STYLES */\r\n"
		+ "    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }\r\n"
		+ "    table { border-collapse: collapse !important; }\r\n"
		+ "    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }\r\n"
		+ "\r\n" + "    /* iOS BLUE LINKS */\r\n"
		+ "    a[x-apple-data-detectors] {\r\n"
		+ "        color: inherit !important;\r\n"
		+ "        text-decoration: none !important;\r\n"
		+ "        font-size: inherit !important;\r\n"
		+ "        font-family: inherit !important;\r\n"
		+ "        font-weight: inherit !important;\r\n"
		+ "        line-height: inherit !important;\r\n"
		+ "    }\r\n" + "    \r\n"
		+ "    /* MOBILE STYLES */\r\n"
		+ "    @media screen and (max-width:600px){\r\n"
		+ "        h1 {\r\n"
		+ "            font-size: 32px !important;\r\n"
		+ "            line-height: 32px !important;\r\n"
		+ "        }\r\n" + "    }\r\n" + "\r\n"
		+ "    /* ANDROID CENTER FIX */\r\n"
		+ "    div[style*=\"margin: 16px 0;\"] { margin: 0 !important; }\r\n"
		+ "</style>\r\n" + "</head>\r\n"
		+ "<body style=\"background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;\">\r\n"
		+ "\r\n" + "<!-- HIDDEN PREHEADER TEXT -->\r\n"
		+ "<div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Lato', Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\r\n"
		+ "    We're thrilled to have you here! Get ready to dive into your new account.\r\n"
		+ "</div>\r\n" + "\r\n"
		+ "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n"
		+ "    <!-- LOGO -->\r\n" + "    <tr>\r\n"
		+ "        <td bgcolor=\"#FFA73B\" align=\"center\">\r\n"
		+ "            <!--[if (gte mso 9)|(IE)]>\r\n"
		+ "            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"600\">\r\n"
		+ "            <tr>\r\n"
		+ "            <td align=\"center\" valign=\"top\" width=\"600\">\r\n"
		+ "            <![endif]-->\r\n"
		+ "            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\" >\r\n"
		+ "                <tr>\r\n"
		+ "                    <td align=\"center\" valign=\"top\" style=\"padding: 40px 10px 40px 10px;\">\r\n"
		+ "                        <a href=\"http://litmus.com\" target=\"_blank\">\r\n"
		+ "                            <img alt=\"Logo\" src=\"http://litmuswww.s3.amazonaws.com/community/template-gallery/ceej/logo.png\" width=\"40\" height=\"40\" style=\"display: block; width: 40px; max-width: 40px; min-width: 40px; font-family: 'Lato', Helvetica, Arial, sans-serif; color: #ffffff; font-size: 18px;\" border=\"0\">\r\n"
		+ "                        </a>\r\n"
		+ "                    </td>\r\n"
		+ "                </tr>\r\n"
		+ "            </table>\r\n"
		+ "            <!--[if (gte mso 9)|(IE)]>\r\n"
		+ "            </td>\r\n" + "            </tr>\r\n"
		+ "            </table>\r\n"
		+ "            <![endif]-->\r\n" + "        </td>\r\n"
		+ "    </tr>\r\n" + "    <!-- HERO -->\r\n"
		+ "    <tr>\r\n"
		+ "        <td bgcolor=\"#FFA73B\" align=\"center\" style=\"padding: 0px 10px 0px 10px;\">\r\n"
		+ "            <!--[if (gte mso 9)|(IE)]>\r\n"
		+ "            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"600\">\r\n"
		+ "            <tr>\r\n"
		+ "            <td align=\"center\" valign=\"top\" width=\"600\">\r\n"
		+ "            <![endif]-->\r\n"
		+ "            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\" >\r\n"
		+ "                <tr>\r\n"
		+ "                    <td bgcolor=\"#ffffff\" align=\"center\" valign=\"top\" style=\"padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;\">\r\n"
		+ "                      <h1 style=\"font-size: 48px; font-weight: 400; margin: 0;\">Welcome!</h1>\r\n"
		+ "                    </td>\r\n"
		+ "                </tr>\r\n"
		+ "            </table>\r\n"
		+ "            <!--[if (gte mso 9)|(IE)]>\r\n"
		+ "            </td>\r\n" + "            </tr>\r\n"
		+ "            </table>\r\n"
		+ "            <![endif]-->\r\n" + "        </td>\r\n"
		+ "    </tr>\r\n" + "    <!-- COPY BLOCK -->\r\n"
		+ "    <tr>\r\n"
		+ "        <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 0px 10px 0px 10px;\">\r\n"
		+ "            <!--[if (gte mso 9)|(IE)]>\r\n"
		+ "            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"600\">\r\n"
		+ "            <tr>\r\n"
		+ "            <td align=\"center\" valign=\"top\" width=\"600\">\r\n"
		+ "            <![endif]-->\r\n"
		+ "            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\" >\r\n"
		+ "              <!-- COPY -->\r\n"
		+ "              <tr>\r\n"
		+ "                <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 20px 30px 40px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\" >\r\n"
		+ "                  <p style=\"margin: 0;\">We're excited to have you get started. First, you need to confirm your account. Just press the button below.</p>\r\n"
		+ "                </td>\r\n"
		+ "              </tr>\r\n"
		+ "              <!-- BULLETPROOF BUTTON -->\r\n"
		+ "              <tr>\r\n"
		+ "                <td bgcolor=\"#ffffff\" align=\"left\">\r\n"
		+ "                  <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n"
		+ "                    <tr>\r\n"
		+ "                      <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 20px 30px 60px 30px;\">\r\n"
		+ "                        <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n"
		+ "                          <tr>\r\n"
		+ "                              <td align=\"center\" style=\"border-radius: 3px;\" bgcolor=\"#FFA73B\"><a href=\""
		+ Faces.getRequestBaseURL() + "v2/service.xhtml?uid="
		+ userId + "&code=" + verifyCode
		+ "\" target=\"_blank\" style=\"font-size: 20px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; text-decoration: none; padding: 15px 25px; border-radius: 2px; border: 1px solid #FFA73B; display: inline-block;\">Confirm Account</a></td>\r\n"
		+ "                          </tr>\r\n"
		+ "                        </table>\r\n"
		+ "                      </td>\r\n"
		+ "                    </tr>\r\n"
		+ "                  </table>\r\n"
		+ "                </td>\r\n"
		+ "              </tr>\r\n"
		+ "              <!-- COPY -->\r\n"
		+ "              <tr>\r\n"
		+ "                <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 0px 30px 0px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\" >\r\n"
		+ "                  <p style=\"margin: 0;\">If that doesn't work, copy and paste the following link in your browser:</p>\r\n"
		+ "                </td>\r\n"
		+ "              </tr>\r\n"
		+ "              <!-- COPY -->\r\n"
		+ "                <tr>\r\n"
		+ "                  <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 20px 30px 20px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\" >\r\n"
		+ "                    <p style=\"margin: 0;\"><a href=\""
		+ Faces.getRequestBaseURL() + "v2/service.xhtml?uid="
		+ userId + "&code=" + verifyCode
		+ "\" target=\"_blank\" style=\"color: #FFA73B;\">"
		+ Faces.getRequestBaseURL() + "v2/service.xhtml?uid="
		+ userId + "&code=" + verifyCode + "</a></p>\r\n"
		+ "                  </td>\r\n"
		+ "                </tr>\r\n"
		+ "              <!-- COPY -->\r\n"
		+ "              <tr>\r\n"
		+ "                <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 0px 30px 20px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\" >\r\n"
		+ "                  <p style=\"margin: 0;\">If you have any questions, just reply to this email—we're always happy to help out.</p>\r\n"
		+ "                </td>\r\n"
		+ "              </tr>\r\n"
		+ "              <!-- COPY -->\r\n"
		+ "              <tr>\r\n"
		+ "                <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\" >\r\n"
		+ "                  <p style=\"margin: 0;\">Cheers,<br>The Ceej Team</p>\r\n"
		+ "                </td>\r\n"
		+ "              </tr>\r\n"
		+ "            </table>\r\n"
		+ "            <!--[if (gte mso 9)|(IE)]>\r\n"
		+ "            </td>\r\n" + "            </tr>\r\n"
		+ "            </table>\r\n"
		+ "            <![endif]-->\r\n" + "        </td>\r\n"
		+ "    </tr>\r\n" + "    <!-- SUPPORT CALLOUT -->\r\n"
		+ "    <tr>\r\n"
		+ "        <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 30px 10px 0px 10px;\">\r\n"
		+ "            <!--[if (gte mso 9)|(IE)]>\r\n"
		+ "            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"600\">\r\n"
		+ "            <tr>\r\n"
		+ "            <td align=\"center\" valign=\"top\" width=\"600\">\r\n"
		+ "            <![endif]-->\r\n"
		+ "            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\" >\r\n"
		+ "                <!-- HEADLINE -->\r\n"
		+ "                <tr>\r\n"
		+ "                  <td bgcolor=\"#FFECD1\" align=\"center\" style=\"padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\" >\r\n"
		+ "                    <h2 style=\"font-size: 20px; font-weight: 400; color: #111111; margin: 0;\">Need more help?</h2>\r\n"
		+ "                    <p style=\"margin: 0;\"><a href=\"http://litmus.com\" target=\"_blank\" style=\"color: #FFA73B;\">We&rsquo;re here, ready to talk</a></p>\r\n"
		+ "                  </td>\r\n"
		+ "                </tr>\r\n"
		+ "            </table>\r\n"
		+ "            <!--[if (gte mso 9)|(IE)]>\r\n"
		+ "            </td>\r\n" + "            </tr>\r\n"
		+ "            </table>\r\n"
		+ "            <![endif]-->\r\n" + "        </td>\r\n"
		+ "    </tr>\r\n" + "    <!-- FOOTER -->\r\n"
		+ "    <tr>\r\n"
		+ "        <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 0px 10px 0px 10px;\">\r\n"
		+ "            <!--[if (gte mso 9)|(IE)]>\r\n"
		+ "            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"600\">\r\n"
		+ "            <tr>\r\n"
		+ "            <td align=\"center\" valign=\"top\" width=\"600\">\r\n"
		+ "            <![endif]-->\r\n"
		+ "            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\" >\r\n"
		+ "              <!-- NAVIGATION -->\r\n"
		+ "              <tr>\r\n"
		+ "                <td bgcolor=\"#f4f4f4\" align=\"left\" style=\"padding: 30px 30px 30px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 18px;\" >\r\n"
		+ "                  <p style=\"margin: 0;\">\r\n"
		+ "                    <a href=\"http://litmus.com\" target=\"_blank\" style=\"color: #111111; font-weight: 700;\">Dashboard</a> -\r\n"
		+ "                    <a href=\"http://litmus.com\" target=\"_blank\" style=\"color: #111111; font-weight: 700;\">Billing</a> -\r\n"
		+ "                    <a href=\"http://litmus.com\" target=\"_blank\" style=\"color: #111111; font-weight: 700;\">Help</a>\r\n"
		+ "                  </p>\r\n"
		+ "                </td>\r\n"
		+ "              </tr>\r\n"
		+ "              <!-- PERMISSION REMINDER -->\r\n"
		+ "              <tr>\r\n"
		+ "                <td bgcolor=\"#f4f4f4\" align=\"left\" style=\"padding: 0px 30px 30px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 18px;\" >\r\n"
		+ "                  <p style=\"margin: 0;\">You received this email because you just signed up for a new account. If it looks weird, <a href=\"http://litmus.com\" target=\"_blank\" style=\"color: #111111; font-weight: 700;\">view it in your browser</a>.</p>\r\n"
		+ "                </td>\r\n"
		+ "              </tr>\r\n"
		+ "              <!-- UNSUBSCRIBE -->\r\n"
		+ "              <tr>\r\n"
		+ "                <td bgcolor=\"#f4f4f4\" align=\"left\" style=\"padding: 0px 30px 30px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 18px;\" >\r\n"
		+ "                  <p style=\"margin: 0;\">If these emails get annoying, please feel free to <a href=\"http://litmus.com\" target=\"_blank\" style=\"color: #111111; font-weight: 700;\">unsubscribe</a>.</p>\r\n"
		+ "                </td>\r\n"
		+ "              </tr>\r\n"
		+ "              <!-- ADDRESS -->\r\n"
		+ "              <tr>\r\n"
		+ "                <td bgcolor=\"#f4f4f4\" align=\"left\" style=\"padding: 0px 30px 30px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 18px;\" >\r\n"
		+ "                  <p style=\"margin: 0;\">Ceej - 1234 Main Street - Anywhere, MA - 56789</p>\r\n"
		+ "                </td>\r\n"
		+ "              </tr>\r\n"
		+ "            </table>\r\n"
		+ "            <!--[if (gte mso 9)|(IE)]>\r\n"
		+ "            </td>\r\n" + "            </tr>\r\n"
		+ "            </table>\r\n"
		+ "            <![endif]-->\r\n" + "        </td>\r\n"
		+ "    </tr>\r\n" + "</table>\r\n" + "    \r\n"
		+ "</body>\r\n" + "</html>";
    }

    public static final String createEmailPlain(String userId,
	    String verifyCode) {
	return "Verify URL: " + Faces.getRequestBaseURL() + "/service.xhtml?uid="
		+ userId + "&code=" + verifyCode;
    }
}
